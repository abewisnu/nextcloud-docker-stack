
![Logo](https://avatars.githubusercontent.com/u/19211038?s=80&v=4)
# Nextcloud - Docker Stack
This is docker compose creates nextcloud, mysql and phpmyadmin in one stack.
## Envrionment
Rename & edit envrionment

```bash
cp sample.env .env
```

```bash
nano .env
```

## Network
Create docker network.

```bash
docker network create NextCloud
```

## Build Docker
```bash
docker-compose up -d
```

## Docker running in the background
```bash
docker-compose ps
```
    
## Authors

- [@abewisnu](https://www.github.com/abewisnu)
## License

[MIT](https://choosealicense.com/licenses/mit/)

